def make_bs_theta_plot(dfn_data, nc_data, layout, ik = 0, il = 0, ie = 0, isp = 0, isig = 0,
                       it_target = 0, colour = None, style = '',
                       label = None, transform = abs):

    """ Produces a ballooning space plot for the supercell containing the
    it_target it index. Should work for flux tube and ballooning space runs.
    """
    #TODO: Add a 'stack' flag which squashes the separate it sections (cells)
    #into a single array (and returns it?). This should probably be implemented
    #as a separate utility and simply called here. Note GS2 implements this
    #functionality so we could simply port the logic.

    from matplotlib.pyplot import plot

    # Sanity check the inputs
    required_entries = ['supercell_labels', 'theta', 'theta0']
    if not all([x in nc_data.keys() for x in required_entries]):
        raise Exception('Not all required keys in nc_data')

    # Work out mapping from dimension to index
    # The dimensions are in reverse order as compared to the layout string
    # so we reverse the layout string here
    layout_list = list(layout)[::-1]
    isp_ind, ie_ind, il_ind, ik_ind, it_ind = [
        layout_list.index(x) for x in ['s','e','l','y','x']]

    from numpy import s_
    def pack_indices(isp,ie,il,ik,it):
        index_pack = list([0,0,0,0,0])
        index_pack[isp_ind] = isp
        index_pack[ie_ind] = ie
        index_pack[il_ind] = il
        index_pack[ik_ind] = ik
        index_pack[it_ind] = it
        return s_[index_pack[0], index_pack[1], index_pack[2],
                  index_pack[3], index_pack[4], isig, :]

    def null(a):
        return a

    if transform is None: transform = null

    nt = nc_data['supercell_labels'].shape[-1]

    it_list = [it for it in range(nt)
               if nc_data['supercell_labels'][ik, it] ==
               nc_data['supercell_labels'][ik, it_target]]

    [plot(nc_data['theta'] - nc_data['theta0'][ik, it],
          transform(dfn_data[pack_indices(isp, ie, il, ik, it)]).T,
          style, label = label, color = colour) for it in it_list]


def make_vpar_vperp_plot(dfn_data, nc_data, layout, ik = 0, it = 0, isp = 0, ig = 0,
                         transform = abs, plot_points = False, edgecolor = None,
                         shade = True, return_data = False):

    """Produces a pcolormesh plot showing the distribution function
    as a function of vperp and vpar for the selected theta grid point,
    species and wavenumber.
    """

    from matplotlib.pyplot import pcolormesh, plot
    from numpy import zeros, sqrt, ones, NaN
    # Sanity check the inputs
    required_entries = ['bmag', 'theta', 'lambda', 'energy']
    if not all([x in nc_data.keys() for x in required_entries]):
        raise Exception('Not all required keys in nc_data')

    # Work out mapping from dimension to index
    # The dimensions are in reverse order as compared to the layout string
    # so we reverse the layout string here
    layout_list = list(layout)[::-1]
    isp_ind, ie_ind, il_ind, ik_ind, it_ind = [
        layout_list.index(x) for x in ['s','e','l','y','x']]

    bmag = nc_data['bmag']
    has_trapped = bmag.max() > bmag.min()
    pitch = nc_data['lambda']
    energy = nc_data['energy'][isp,:]
    nenergy = energy.shape[0]
    bmag_local = bmag[ig]

    npitch = pitch.shape[0]
    nvalid_pitch = 0
    for ip, pitch_val in enumerate(pitch):
        tmp = bmag_local * pitch_val
        if tmp <= 1:
            nvalid_pitch += 1
        else:
            break

    # Two signs of vpar for each pitch angle
    nvpar = 2*nvalid_pitch
    if has_trapped:
        # Except for vpar = 0
        nvpar -= 1

    from numpy import s_
    def pack_indices(isp,ie,il,ik,it,isig,ig):
        index_pack = list([0,0,0,0,0])
        index_pack[isp_ind] = isp
        index_pack[ie_ind] = ie
        index_pack[il_ind] = il
        index_pack[ik_ind] = ik
        index_pack[it_ind] = it
        return s_[index_pack[0], index_pack[1], index_pack[2],
                  index_pack[3], index_pack[4], isig, ig]

    # We add an extra energy row to provide an energy = 0
    # set of grid points. This helps avoid pcolormesh
    # issues due to not knowing the lower bounds of the
    # cells it draws.
    vpar = zeros([nvpar, nenergy + 1])
    vperp = zeros(vpar.shape)
    data = zeros(vpar.shape, dtype = 'complex')

    for ip, pitch_val in enumerate(pitch):
        tmp = bmag_local * pitch_val
        if tmp <= 1.0:
            for ie, energy_val in enumerate(energy):
                vperp[ip, ie + 1] = sqrt(energy_val*tmp)
                vpar[ip, ie + 1] = -sqrt(energy_val * (1-tmp))
                data[ip, ie + 1] = dfn_data[pack_indices(isp, ie, ip, ik, it, 1, ig)]
                if not has_trapped or (has_trapped and ip < nvalid_pitch):
                    vperp[-ip-1, ie + 1] = vperp[ip, ie + 1]
                    vpar[-ip-1, ie + 1] = -vpar[ip, ie + 1]
                    data[-ip-1, ie + 1] = dfn_data[pack_indices(isp, ie, ip, ik, it, 0, ig)]
    data[:, 0] = NaN

    def null(a):
        return a

    if transform is None: transform = null

    if shade:
        shading = 'gouraud'
    else:
        shading = 'auto'

    pcolormesh(vpar, vperp, transform(data), shading = shading, edgecolor = edgecolor)

    if plot_points:
        plot(vpar, vperp, 'kx')

    if return_data:
        return vpar, vperp, data
