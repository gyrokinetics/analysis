"""
Routines for determining the growth rate and frequncy using different methods.
Designed to match the original IDL routine in terms of behaviour.
"""

#Growth rates
def __get_gam_fit(dat=None,key='phi2',quiet=False,*args,**kwargs):
    """
    Determine growth rate from fit to data.
    """
    from numpy import polyfit, log, linspace, isfinite

    if dat is None:
        raise ValueError("Must pass in dat dictionary.")

    __keyFallbacks = ['phi2','apar2','bpar2']
    
    #Check if the key is in the data dictionary
    if key not in dat:
        if not quiet: print("Warning: Variable {k} not found in the data dictionary -- using fallbacks.".format(k=key))
        for key in __keyFallbacks:
            if key not in dat:
                if not quiet: print("Fallback {k} not found".format(k=key))
            else:
                break

    #Extract the data
    fDat = dat[key]

    #Filter out non-finite data
    ind = linspace(0,len(fDat)-1,len(fDat),dtype='int')
    indices = ind[isfinite(log(fDat))]
    fDat = fDat[indices]

    #Get the time data
    tDat = dat['t'][indices]

    #Now we need to decide which section of the data we want to fit over
    #For now we'll just use the last n%
    nTime = len(tDat)
    nFunc = len(fDat)
    if nTime != nFunc:
        print("Warning: Function and Time array have different lengths -- truncating to minimum")
        nMin  = min([nTime,nFunc])
        nTime = nMin
        nFunc = nMin

    percentUse = 25.0
    nToUse = int( nTime * (percentUse/100.0))
    nToUse = max(nToUse, 4)
    nToUse = min(nToUse, nTime)
    
    fDat = fDat[-nToUse:]
    tDat = tDat[-nToUse:]

    #Fit
    fitPar = polyfit(tDat, log(fDat), 1)
    gamma = 0.5*fitPar[0]
    
    return gamma

def __get_gam_eig(dat=None,eigInd=None,*args,**kwargs):
    """
    Determine growth rate from output array.
    """
    from ..utils import pickOption

    if dat is None:
        raise ValueError("Must pass in dat dictionary.")
    if 'omega' not in dat:
        raise KeyError("Data dictionary doesn't contain omega array.")

    #Count how many eigenvalues are available
    nEig = len(dat['omega'])

    if eigInd>=nEig or eigInd<0:
        print("Warning: Invalid eigInd : {e}".format(e=eigInd))
        eigInd = None

    #Now pick which eig if not selected or invalid selection
    if eigInd is None:
        eigInd = pickOption(dat['omega'],sort=False)

    return dat['omega'][eigInd].imag

def __get_gam_dat(dat=None,*args,**kwargs):
    """
    Determine growth rate from output array.
    """
    from numpy import linspace, isfinite, squeeze
    if dat is None:
        raise ValueError("Must pass in dat dictionary.")
    if 'omega' not in dat:
        raise KeyError("Data dictionary doesn't contain omega array.")

    fDat = squeeze(dat['omega'].imag)
    ind = linspace(0,len(fDat)-1,len(fDat),dtype='int')
    indices = ind[isfinite(fDat)]

    return fDat[indices][-1]

def __get_gam_datAvg(dat=None,*args,**kwargs):
    """
    Determine growth rate from averaged output array.
    """
    from numpy import linspace, isfinite, squeeze
    if dat is None:
        raise ValueError("Must pass in dat dictionary.")
    if 'omegaavg' not in dat:
        raise KeyError("Data dictionary doesn't contain omegaavg array.")

    fDat = squeeze(dat['omegaavg'].imag)
    ind = linspace(0,len(fDat)-1,len(fDat),dtype='int')
    indices = ind[isfinite(fDat)]

    return fDat[indices][-1]

#Frequencies
def __get_freq_eig(dat=None,eigInd=None,*args,**kwargs):
    """
    Determine frequency from output array.
    """
    from ..utils import pickOption

    if dat is None:
        raise ValueError("Must pass in dat dictionary.")
    if 'omega' not in dat:
        raise KeyError("Data dictionary doesn't contain omega array.")

    #Count how many eigenvalues are available
    nEig = len(dat['omega'])

    if eigInd>=nEig or eigInd<0:
        print("Warning: Invalid eigInd : {e}".format(e=eigInd))
        eigInd = None

    #Now pick which eig if not selected or invalid selection
    if eigInd is None:
        eigInd = pickOption(dat['omega'],sort=False)

    return dat['omega'][eigInd].real

def __get_freq_dat(dat=None,*args,**kwargs):
    """
    Determine frequency from output array.
    """
    from numpy import linspace, isfinite, newaxis
    if dat is None:
        raise ValueError("Must pass in dat dictionary.")
    if 'omega' not in dat:
        raise KeyError("Data dictionary doesn't contain omega array.")

    return dat['omega'].real[-1, :, :]

def __get_freq_datAvg(dat=None,*args,**kwargs):
    """
    Determine frequency from averaged output array.
    """
    from numpy import linspace, isfinite
    if dat is None:
        raise ValueError("Must pass in dat dictionary.")
    if 'omegaavg' not in dat:
        raise KeyError("Data dictionary doesn't contain omegaavg array.")

    return dat['omegaavg'].real[-1, :, :]    

#Wrappers
def get_gam(dat=None,eig=False,useFit=True,useAvg=False,*args,**kwargs):
    """
    Return the growth rate.
    """
    if eig:
        return __get_gam_eig(dat,*args,**kwargs)
    elif useFit:
        return __get_gam_fit(dat,*args,**kwargs)
    else:
        if useAvg:
            return __get_gam_datAvg(dat,*args,**kwargs)
        else:
            return __get_gam_dat(dat,*args,**kwargs)

def get_freq(dat=None,eig=False,useAvg=False,*args,**kwargs):
    """
    Return the frequency.
    """
    
    if eig:
        return __get_freq_eig(dat,*args,**kwargs)
    elif useAvg:
        return __get_freq_datAvg(dat,*args,**kwargs)
    else:
        return __get_freq_dat(dat,*args,**kwargs)

#Main entry -- by default just return growth rate.
def get_omega(dat=None,frequency=False,*args,**kwargs):
    """
    Return the growth rate/complex frequency.
    """
    gam = get_gam(dat,*args,**kwargs)

    if frequency:
        freq = get_freq(dat,*args,**kwargs)
        result = freq + complex(0.0,1.0)*gam
    else:
        result = gam

    return result

