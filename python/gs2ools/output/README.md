## Short summary of files provided here

* get_gs2out.py -- reads a GS2 output file into a dictionary. Currently doesn't do anything more than this but in the long run we can use this to also perform standard pre-processing.
* get_distfn.py -- a first stab at a python routine to read in the distribution function from distributed restart files. Don't expect this to work currently.