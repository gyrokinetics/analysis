def get_distfn(pat="*.nc.[0-9]*",weight=True,maxwellMult=False,*args,**kwargs):
    """
    A routine for reading in the distfn from restart files and returning the
    full distfn array merged from the different files.

    Attempts to find all files matching pat. The default ('*.nc.[0-9]') should match the
    usual restart files, however this may not work well for eigenvalue restart files (in
    the case where multiple eigenmodes have been found) and it may be more appropriate to
    use pat='*.eig_<eigInd>.nc.[0-9]*', where <eigInd> is replaced by the index of the
    eigenmode of interest.

    If weight is True then we multiply g by the velocity grid weights (w and wl). This
    can help improve the clarity of the plotted data by suppressing grid points with
    little vspace volume/area (which therefore have higher amplitude for ~uniform dist fn).

    If maxwellMult is True then we multiply g by a Maxwellian in order to (partially?) undo
    the normalisation in the code.

    PROBABLY NEED TO DEAL WITH FORBIDDEN REGIONS, E.G. VPA/VPE GRIDS AT CERTAIN THETA MAY
    BE DISTORTED/CONTAIN FORBIDDEN POINTS ETC.
    """

    from ..utils import ncdf2dict, getFiles
    from numpy import zeros, exp

    #First get a list of matching files
    files = getFiles(pat,**kwargs)
    if files is None:
        raise ValueError("No files found matching '"+pat+"'")

    fileDat = []
    for fil in files:
        fileDat.append(ncdf2dict(fil))

    #Extract the global grids
    theta   = fileDat[0]['theta']   ; ntheta = len(theta)
    sigma   = fileDat[0]['sigma']   ; nsigma = len(sigma)
    lambd   = fileDat[0]['lambda']  ; nlambd = len(lambd)
    energy  = fileDat[0]['energy']  ; nenergy = len(energy)
    species = fileDat[0]['species'] ; nspecies = len(species)

    grids = {'theta':theta,'sigma':sigma,'lambda':lambd,'energy':energy,'species':species}

    g = zeros([nspecies,nenergy,nlambd,nsigma,ntheta],dtype='complex128')
    vpa = zeros([nspecies,nenergy,nlambd,nsigma,ntheta])
    vperp2 = zeros([nspecies,nenergy,nlambd,ntheta])

    for dat in fileDat:
        #Get local data ranges, offset min by one as python indexes from 0
        #but don't offset max as ranges not inclusive at upper end
        il_min = dat['il_min']-1 ; il_max = dat['il_max']
        ie_min = dat['ie_min']-1 ; ie_max = dat['ie_max']
        is_min = dat['is_min']-1 ; is_max = dat['is_max']

        #Store data for this file in global array, note always have
        #all sigma and all theta currently
        g[is_min:is_max, ie_min:ie_max, il_min:il_max, :, :] = dat['g']
        vpa[is_min:is_max, ie_min:ie_max, il_min:il_max, :, :] = dat['vpa']
        vperp2[is_min:is_max, ie_min:ie_max, il_min:il_max, :] = dat['vperp2']

    if weight:
        wfac = zeros([nenergy,nlambd,ntheta])
        we =  fileDat[0]['we']
        wl =  fileDat[0]['wl']
        #Form compound weight array
        for ie, w in enumerate(we):
            wfac[ie,:,:] = w*wl
        #Weight the dist fn
        for indSpec in xrange(nspecies):
            g[indSpec,:,:,0,:] *= wfac
            g[indSpec,:,:,1,:] *= wfac

    if maxwellMult:
        maxwell=exp(-energy)
        for ie in xrange(nenergy):
            g[:,ie,...] *= maxwell[ie]

    vel = {'vpa':vpa, 'vperp2':vperp2}
    ans ={'grids':grids, 'g':g, 'vel':vel}

    return ans
