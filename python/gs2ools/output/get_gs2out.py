
def get_gs2out(filename="*.out.nc",*args,**kwargs):
    """
    A simple routine to read in a GS2 output netcdf file and return
    a suitable dictionary.
    """

    from ..utils import ncdf2dict, getFile
#    from ..input import input2dict, defaultInp

    #First check the file can be found
    fileNm = getFile(filename,**kwargs)

    #Now read in the data
    dat = ncdf2dict(fileNm, **kwargs)

    #Now decide what the input file should be called
    #/Complicated handling to deal with case where filename
    #contains multiple instances of .out.nc -- we just want to 
    baseNm = ".out.nc".join(filter(None,fileNm.split(".out.nc")))

    #Now we want to see if we can read the matching input file
    inpNm = baseNm.replace("_eig","")+".in"
    try:
        inpNm = getFile(inpNm,**kwargs)
        filFound = True
    except IOError:
        print("Couldn't find input file {f} -- using default values".format(f=inpNm))
        filFound = False

    # if filFound:
    #     inp = input2dict(inpNm)
    # else:
    #     inp = defaultInp()

    # #Now we can process the data
    # #--> Adjust the time domain
    # dat['t'][1:] = dat['t'][1:] - 0.5*inp['knobs']['delt']
    # #--> Insert additional data into return
    # #dat['gexb'] = ....
    # #dat['delt'] = inp['knobs']['delt']


    retDat = dat

    return retDat
    
