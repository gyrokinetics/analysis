def _get_and_combine_data(key, file_data):
    """
    Small internal helper for extracting and combining data from multiple files.
    """
    from numpy import vstack
    data = file_data[0][key]
    for case in file_data[1:]:
        data = vstack([data, case[key]])
    return data

def read_restart(fold="./", pat="*.nc.[0-9]*", data_name = 'g', complex_data = True, include_fields = True, verbose = True, **kwargs):
    """
    A routine for reading in the distfn from restart files and returning the
    full distfn array merged from the different files.

    Attempts to find all files matching pat in directory fold. The
    default ('*.nc.[0-9]') should match the usual restart files,
    however this may not work well for eigenvalue restart files (in
    the case where multiple eigenmodes have been found) and it may be
    more appropriate to use pat='*.eig_<eigInd>.nc.[0-9]*', where
    <eigInd> is replaced by the index of the eigenmode of interest.

    Designed to work with restart files written by GS2 8.1 or newer
    """

    from ..utils import ncdf2dict, getFiles, numFromStr
    from os import path

    # First get a list of matching files
    file_pattern = path.join(fold, pat)
    if verbose: print("Searching for files matching {pat}".format(pat=file_pattern))
    files = getFiles(file_pattern,**kwargs)
    if files is None:
        raise ValueError("No files found matching '"+file_pattern+"'")

    if verbose: print("Found {n} restart files.".format(n=len(files)))

    # Really need to sort the files found to ensure that they are in
    # the expected order.
    # Sort the files based on the last number in the matching file names.
    # This may not always work but is a reasonable attempt at something
    # reasonable. Single restart file cases may need a fallback
    files = sorted(files, key = lambda x: int(numFromStr(x)[-1]))

    # Read in the data. We currently don't try to minimise memory
    # usage and just read everything in at once.
    file_data = []
    for restart_file in files:
        if verbose > 1: print("\tReading {f}".format(f=restart_file))
        file_data.append(ncdf2dict(restart_file))

    if 'layout' not in file_data[0]:
        raise KeyError("No 'layout' value found in {fil} - only support restart files from GS2 8.1 and newer".format(fil=files[0]))

    # Now work out the overall layout and shape of the distribution
    # function.
    layout = [x.decode('utf-8') for x in file_data[0]['layout']]
    layout_str = ''.join(layout)
    # Note we add zero to get data out of masked array
    nx = file_data[0]['ntheta0']+0
    ny = file_data[0]['naky']+0
    nl = file_data[0]['nlambda']+0
    ne = file_data[0]['negrid']+0
    ns = file_data[0]['nspec']+0
    nsigma = 2
    ntheta = file_data[0]['gr'].shape[2]

    grids = { 'x':nx, 'y':ny, 'l':nl, 'e':ne, 's':ns}

    the_shape = []
    for char in layout:
        the_shape.append(grids[char])

    if verbose: print("Compound dimension using layout '{lay}' with shape {sh}".format(
            lay = layout_str, sh = the_shape))

    # Determine the full output shape
    full_shape = the_shape[::-1]
    full_shape.extend([nsigma, ntheta])

    if verbose: print("The output shape will be {sh}".format(sh =  full_shape))

    # Now stack up distribution function data
    if complex_data:
        if verbose: print("Reading {d}r and {d}i".format(d=data_name))
        data = (_get_and_combine_data(data_name+'r', file_data) +
             complex(0, 1.0) * _get_and_combine_data(data_name+'i', file_data)).reshape(full_shape)
    else:
        if verbose: print("Reading {d}".format(d=data_name))
        data = _get_and_combine_data(data_name, file_data)

    fields = {}
    if include_fields:
        if verbose: print("Reading fields.")
        if 'phi_r' in file_data[0].keys():
            if verbose: print("\tPhi")
            fields['phi'] = (file_data[0]['phi_r'] +
                             complex(0, 1.0) * file_data[0]['phi_i'])
        if 'apar_r' in file_data[0].keys():
            if verbose: print("\tApar")
            fields['apar'] = (file_data[0]['apar_r'] +
                              complex(0, 1.0) * file_data[0]['apar_i'])
        if 'bpar_r' in file_data[0].keys():
            if verbose: print("\tBpar")
            fields['bpar'] = (file_data[0]['bpar_r'] +
                              complex(0, 1.0) * file_data[0]['bpar_i'])
    return data
