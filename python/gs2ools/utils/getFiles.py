
def getFiles(filePat="*",fileOnly=False,directoryOnly=False,*args,**kwargs):
    """
    Get a list of all matching files
    """
    
    from glob import glob
    import os

    matches = glob(filePat)

    if directoryOnly:
        matches = [x for x in matches if os.path.isdir(x)]
    elif fileOnly:
        matches = [x for x in matches if os.path.isfile(x)]

    if len(matches) == 0:
        matches = None

    return matches
