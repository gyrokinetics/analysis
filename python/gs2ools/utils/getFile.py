
def getFile(filePat="*",fatal=True,*args,**kwargs):
    """
    Get a single file matching pattern 'filePat'.
    """

    from . import getFiles, pickOption

    fileMatch = getFiles(filePat,**kwargs)

    #Handle the no match case
    if fileMatch is None and fatal:
        raise IOError("Couldn't find any files matching {s}".format(s=filePat))
    elif fileMatch is None:
        return None

    #Only one match so return that
    if len(fileMatch) == 1:
        return fileMatch[0]

    #Multiple matches -- ask user to pick
    filePick = pickOption(fileMatch)

    return fileMatch[filePick]
