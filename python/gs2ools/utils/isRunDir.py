
def isRunDir(fold=None,fileReq=[],eig=False,*args,**kwargs):
    """
    Decide if passed folder is a valid run directory or not.
    Based on the presence of certain files.
    """
    import os
    from glob import glob

    if not os.path.isdir(fold):
        return False

    #A list of files that must be present 
    if eig:
        files = ["*_eig.out.nc"]
    else:
        files = ["*.out.nc"]
    files.extend(fileReq)

    for f in files:
        if len(glob(os.path.join(fold,f))) == 0:
            return False

    #If we get here then folder is judged to be a run directory
    return True
