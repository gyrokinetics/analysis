
def pickOption(opts,indexStart=1,nTry=5,prompt="Pick an option",sort=True,verbose=False):
    """
    Ask the user to select an option from a list.
    """
    from .twoThreeCompat import myinput as input

    defaultValue = indexStart

    #Handle special cases
    if len(opts) == 0: #No options
        raise ValueError("No options to pick from.")

    if len(opts) == 1: #Only one option
        if verbose:
            print("Only one option so selecting " + str(opts[0]))
        return indexStart

    indexEnd = len(opts) + indexStart - 1

    #Ensure the same set of options always in the same order
    ind = range(len(opts))
    origList = dict(zip(opts,ind))
    if sort:
        indices =  [x for (y,x) in sorted(zip(opts,ind), key = lambda pair: pair[0])]
    else:
        indices =  ind
    optsInt = [opts[i] for i in indices]

    print("From the following list:")
    for i in range(0, len(optsInt)):
        print("  {i}\t: {v}".format(i=i+indexStart,v=optsInt[i]))
    
    count = 0
    pick = None
    while count<nTry:
        count = count+1
        try:
            pick = int(input(prompt+" : "))
        except ValueError:
            print("Please enter an integer")
        else:
            if pick >= indexStart and pick <= indexEnd:
                break
            else:
                print("Selection out of range -- try again.")
                pick = None    
    
    if pick is None:
        raise Exception("No valid pick selected within maximum tries ({v})".format(v=nTry))

    #Now get original index
    return origList[optsInt[pick-indexStart]]
