
def numFromStr(strIn,repDict={'p':'.','m':'-'},*args,**kwargs):
    """
    Extract all numbers from a string
    """
    import re
    re_float = re.compile(r'([-]?\d+\.?\d*(e[-]?\d+)?)')

    #Make any replacements required
    for k, v in repDict.items():
        strIn = strIn.replace(k,v)

    # This returns a list of tuples, with the first entry being a number
    # possibly in exponential notation, with the second being any found
    # exponential.
    res = re.findall(re_float, strIn)

    # Here we only keep the first entry, corresponding to the full number
    res = [ x[0] for x in res]
    return res

