
def isV3():
    import sys
    if sys.version_info >= (3, 0):
       return True
    else:     
       return False

def isV2():
    import sys
    if sys.version_info < (3, 0) and sys.version_info>=(2,0):
       return True
    else:     
       return False

def myinput(*args,**kwargs):
    if isV3():
        return input(*args,**kwargs)
    else:
        return raw_input(*args,**kwargs)
