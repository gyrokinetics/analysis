
def __basicScan(fold,eig=False,*args,**kwargs):
    """
    Just read in the netcdf file and unpack a few bits
    """
    from ..output import get_gs2out
    from ..analysis import get_omega
    if eig:
        eigStr="_eig"
    else:
        eigStr=""
    dat=get_gs2out(fold+"/*{e}.out.nc".format(e=eigStr),*args,**kwargs)
    try:
        om = get_omega(dat,eig=eig,*args,**kwargs)
    except:
        print("Exception whilst getting omega -- set to 0.")
        om = 0.
    ret={}
    ret['data'] = dat
    ret['omega'] = om
    return ret

def __reshape(dat=None,axesLen=None,numsVary=None):
    """
    Reshape data to match axesLen. Can't always just use a simple reshape
    e.g. when the scan is incomplete.
    """
    from numpy import product
    #Check if we can get away with a simple reshape
    if product(axesLen) == len(dat):
        pass
    
def getScan(pat="*",analysisCallback=None,*args,**kwargs):
    """
    Given a pattern of directories we find all matching folders and
    then analyse each run.
    """

    from . import getFiles, numFromStr, isRunDir
    from numpy import zeros, linspace, array, reshape

    #First find the matching directories
    folds = getFiles(pat,directoryOnly=True)
    if folds is None:
        print("Warning: No directories found matching '{v}'".format(v=pat))
        return None

    #Filter out non-run folders
    #folds = filter(isRunDir,folds)
    nFold = len(folds)

    #Now find out what numbers vary in the folders
    nums = [numFromStr(x,*args,**kwargs) for x in folds]
    nNums = [len(x) for x in nums]

    if len(set(nNums)) != 1:
        print("Warning: Different amount of numbers in each directory name -- can't order runs.")
        nNums = 0
    else:
        nNums = nNums[0]

    #Now we want to work out which numbers vary between runs
    #--> This then allows us to work out the dimensions of the scan
    if nNums != 0:
        #First unpack into an array
        numsArr = zeros([nFold,nNums])
        for i,num in enumerate(nums):
            numsArr[i,:] = [float(x) for x in num]

        #Now we can work out which numbers vary
        varies = []
        for i in range(nNums):
            varies.append(len(set(numsArr[:,i]))>1)
        nVary = sum(varies)
        
        #If no numbers vary then not much else to do
        if nVary == 0:
            print("Warning: No numbers vary in directory name.")
        else:
            numsVary = zeros([nFold,nVary])
            count = 0
            for i in range(nNums):
                if varies[i]:
                    numsVary[:,count] = numsArr[:,i]
                    count = count+1

            #Here we construct indices with which we can sort
            #the folders etc. based on the numbers that vary
            inds = linspace(0,nFold-1,nFold,dtype='int')
            s = sorted(inds, key=lambda x:tuple(numsVary[x,:]))
            numsVary = numsVary[s,:]
            folds = [folds[x] for x in s]

            axes = []
            for i in range(nVary):
                #Note need sorted as set does not maintain order
                axes.append(array(sorted(list(set(numsVary[:,i])))))
            axesLen = [len(x) for x in axes]
    else:
        nVary = 0


    if nVary == 0:
        axes=[]
        axesLen=[nFold]

    #Now we want to just do our analysis
    results=[]
    useDefaultAnalysis = False
    if analysisCallback is None:
        useDefaultAnalysis = True
        analysisCallback = __basicScan
    for i, fold in enumerate(folds):
        if(isRunDir(fold,*args,**kwargs)):
            results.append(analysisCallback(fold,*args,**kwargs))
        else:
            results.append({})
    #Create return dict
    ret = {}
    ret['folds'] = reshape(array(folds),axesLen)
    ret['nFold'] = nFold
    ret['nVary'] = nVary
    ret['axes']  = axes
    ret['data']  = reshape(array(results),axesLen)

    #If we know we're using the default analysis we can unpack some useful data
    #that we know will be provided
    if useDefaultAnalysis:
        try:
            ty = results[0]['omega'].dtype
        except:
            ty = 'complex'
        omega = zeros(nFold, dtype = ty)
        ky = zeros(nFold)
        torN = zeros(nFold)
        for i, r in enumerate(results):
            if 'omega' in r:
                omega[i] = r['omega']
            if 'data' not in r:
                continue
            if 'ky' in r['data']:
                ky[i] = r['data']['ky']
            if 'tor_n' in r['data']:
                torN[i] = r['data']['tor_n']

        ret['omega'] = omega.reshape(axesLen)
        ret['ky'] = ky.reshape(axesLen)
        ret['torN'] = torN.reshape(axesLen)
    return ret
