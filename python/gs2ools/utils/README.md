## Short summary of files provided here

* ncdf2dict.py -- provides a simple wrapper around several possible python libraries for reading from netcdf files. Reads entire files into a python dictionary (mostly from BOUT++)
* getFile.py -- finds a single file that matches a provided pattern.
* getFiles.py -- finds all files that match a provided pattern.
* pickOption.py -- prompts the user to select an item from a list
* twoThreeCompat.py -- some routines for dealing with things in a python version agnostic way.
* numFromStr.py -- try to produce a number from a string after making user specified replacements (e.g. to map "p"-->".")
* isRunDir.py -- tries to determine if the specified directory "is a run directory", this currently means does it contain an output file.
* getScan.py -- tries to automatically apply a user provided analysis routine to the results of some n-dimensional scan. Note : Currently won't work as tries to use other routines not yet available.